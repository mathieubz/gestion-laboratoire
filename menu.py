import json

# fonction de creation de menu
def creer_menu():
    menu=dict()
    return menu

# fonction pour entrer une ligne dans le menu
def ajouter_entree(menu, affichage, fonction, raccourci):
    liste_entree=[raccourci, affichage, fonction]
    menu[str(raccourci)]=liste_entree

# fonction pour afficher un menu
def _afficher_menu(menu):
    print("Voulez vous : ")
    for cle, valeur in menu.items():
        print(f'{cle}: {valeur[1]}')

# fonction pour interagir avec le menu
def _utiliser_menu(menu,quitter):
    while quitter==False:
        choix = input("Entrez votre choix: ")
        if choix in menu:
            quitter=menu[choix][2]()
        else:
            print("Je n'ai pas compris")
        if quitter != True:
            quitter = False
        return quitter
 
# fonction pour gerer l'affichage et l'utilisation du menu
def gerer_menu(menu, quitter):
    while quitter==False:
        _afficher_menu (menu)
        quitter=_utiliser_menu(menu,quitter)        



