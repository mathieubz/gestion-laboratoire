import csv

'''regroupe l'ensemble des fonctions liées à la gestion du laboratoire'''

class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass

#fonction pour créer un labo vide
def labo_vide():
    labo= dict()
    return labo

#fonction de test de présence pour relever l'exception PresentException
def test_nom_present(labo, nom):
    if nom in labo:
        raise PresentException(nom)

#fonction de test de présence pour relever l'exception AbsentException
def test_nom_absent(labo, nom):
    if nom not in labo:
        raise AbsentException(nom)


#fonction pour attribuer un bureau à une personne dans le laboratoire
def enregistrer_arrivée(labo , nom, bureau):
    if nom in labo:
        raise PresentException(nom)

    labo[nom]= bureau

#fonction pour supprimer une personne du le laboratoire
def enregistrer_depart(labo , nom):
    if nom not in labo:
        raise AbsentException(nom)

    del labo[nom]

#fonction de changement d'affectation de bureau pour une personne du laboratoire
def changer_bureau(labo, nom, bureau):
    if nom not in labo:
        raise AbsentException(nom)
    labo[nom]= bureau


#fonction de changement de nom pour une personne du laboratoire
def changer_nom(labo, nom, nouveau_nom):
    if nouveau_nom in labo:
        raise PresentException(nom)
    bureau = labo[nom]
    del labo[nom]
    labo[nouveau_nom]= bureau

#fonction de test pour verifier la presence d'une personne dans le laboratoire
def savoir_membre(labo, personne):
    if personne in labo:
        print(f'{personne} est membre du laboratoire.')
    else:
        print(f"{personne} n'est pas membre du laboratoire.")

#fonction de test pour donner le bureau d'un membre du laboratoire
def savoir_bureau(labo, personne):
    if personne in labo:
        print(f'{personne} occupe le bureau {labo[personne]}')
    else:
        print(f"{personne} n'est pas membre du laboratoire.")


#fonction d'affichage de l'occupation des bureaux sous forme HTML
def afficher_occupation(labo):
    
    ##recuperation des données du bureau sous forme d'un dictionnaire dict(bureau:(ensemble d'occupants))
    labo_inverse= dict()
    for cle, valeur in labo.items():
        
        '''if valeur not in labo_inverse:
        labo_inverse[valeur] = set()
        labo_inverse[valeur].add(cle)'''## version plus longue. D'abord initialisé si vide ensuite rajouter clé dans ensemble
        labo_inverse.setdefault(valeur,set()).add(cle) ##version courte setdefault de valeur n'existe pas alors l'entrée est créee avec un ensemble vide et la clé==>valeur est ajoutée ensuite

    print('''<HTML>
<HEAD><TITLE>Occupation laboratoire</TITLE></HEAD>
<BODY>
<P>''')
    for cle, valeur in sorted(labo_inverse.items()):
        print(f'{cle}: ')
        for valeur in sorted(valeur):
            print (f' -{valeur} ')
    print('''</P>
</BODY></HTML>''')    

#fonction permettant l'importation de membres dans un laboratoire existant à partir d'un fichier csv
def importer_csv(laboratoire,file):
    dict_anomalies=dict() #suivi des membres du laboratoire déjà présents pour lesquels l'attribution du bureau est differente entre le fichier csv et l'enregistrement préalable
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['Nom'] in laboratoire:
                if laboratoire[row['Nom']] != row['Bureau']:
                    liste=[laboratoire[row['Nom']],row['Bureau']]
                    dict_anomalies[row['Nom']]=liste
            else: 
                laboratoire[row['Nom']]=row['Bureau']
        print (dict_anomalies)