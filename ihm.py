import labo
import menu
import csv
import json

def main():

    '''Gerer l'occupation des bureaux dans un laboratoire
    Exemples:   
    arrivée de Xavier en F305
    arrivée d'Amelie en F307
    arrivée de Marc en F305
    afficher le listing du personnel
    ...'''
    #Comment Gerer l'occupation des bureaux dans un laboratoire?
    #Initialiser le labo
    laboratoire = labo.labo_vide()

    #charger les données du labo enregistrées dans le fichier json
    try:
        with open('datas.json', 'r') as f:
            laboratoire = json.load(f)
    except:
        pass

    quitter=False
    #fonction pour imprimer la liste du personnel du laboratoire et les bureaux associés
    def impression_personnel():
        for key, value in laboratoire.items():
            print(f"{key} occupe le bureau {value}")

    #fonction pour sortir de l'interface
    def quitterinter():
        print("Au revoir")
        quitter = True
        return quitter

    labo.importer_csv(laboratoire,'test_importation.csv')

    #Initialiser le menu
    menucurr = menu.creer_menu()

    #entrer les données dans le menu
    menu.ajouter_entree(menucurr, "enregistrer une arrivée", ihmarrivee(laboratoire), 1)
    menu.ajouter_entree(menucurr, "enregistrer un départ", ihmdepart(laboratoire), 2)
    menu.ajouter_entree(menucurr, "modifier le bureau d'une personne", ihmbureau(laboratoire), 3)
    menu.ajouter_entree(menucurr, "modifier le nom d'une personne", ihmnom(laboratoire), 4)
    menu.ajouter_entree(menucurr, "savoir si une personne est membre du laboratoire", ihmmembre(laboratoire), 5)
    menu.ajouter_entree(menucurr, "connaitre le bureau d'une personne", ihmbureau(laboratoire), 6)
    menu.ajouter_entree(menucurr, "imprimer la liste du personnel et les bureaux qu'ils occupent", impression_personnel, 7)
    menu.ajouter_entree(menucurr, "afficher l'occupation des bureaux", ihmoccup(laboratoire), 8)
    menu.ajouter_entree(menucurr, "quitter l'interface", quitterinter, 0)

    menu.gerer_menu(menucurr, quitter)
    
    #Sauvergarder les données dans un fichier .json avant de quitter
    with open('datas.json', 'w') as bck:
        json.dump(laboratoire, bck)






def main_ancien():
    '''Gerer l'occupation des bureaux dans un laboratoire
    Exemples:   
    arrivée de Xavier en F305
    arrivée d'Amelie en F307
    arrivée de Marc en F305
    afficher le listing du personnel
    ...'''
    #Comment Gerer l'occupation des bureaux dans un laboratoire?
    #Initialiser le labo
    laboratoire = labo.labo_vide()
    quitter =  False 


    #fonction pour imprimer la liste du personnel du laboratoire et les bureaux associés
    def impression_personnel():
        for key, value in laboratoire.items():
            print(f"{key} occupe le bureau {value}")

    while not quitter:

        #Afficher le menu
        print ("""Que voulez vous faire ?
        1 - enregistrer une arrivée
        2 - enregistrer un départ
        3 - changer une personne de bureau
        4 - changer le nom d'une personne
        5 - savoir si une personne est membre du laboratoire
        6 - connaitre le bureau d'une personne
        7 - imprimer la liste du personnel et les bureaux qu'ils occupent
        8 - afficher l'occupation des bureaux
        0 - quitter l'interface""")
        
        #Demander le choix
        choix = int(input ("Quel est votre choix?"))

        #Traiter le choix
        if choix == 1: #arrivée

            #demander le nom
            nom = input ("Quel est le nom du nouvel arrivant? ")

            try:
                labo.test_nom_present(laboratoire , nom)
            except labo.PresentException:
                print("Cette personne est deja dans le laboratoire")
            #demander le bureau
            bureau = input ("Quel est le bureau du nouvel arrivant? ")
            
            #enregistrer la nouvelle personne

            labo.enregistrer_arrivée(laboratoire , nom, bureau)


        elif choix ==2 :
        
         #demander le nom
            nom = input ("Quel est le nom de la personne qui s'en va? ")
            try:
                labo.enregistrer_depart(laboratoire , nom)
            except labo.AbsentException:
                    print("Cette personne n'est pas dans le laboratoire")

        elif choix ==3 :

            #demander le nom
            nom = input ("Quel est le nom de la personne qui change de bureau? ")
            try:
                labo.test_nom_absent(laboratoire , nom) 
            except labo.AbsentException:
                    print("Cette personne n'est pas dans le laboratoire")
            #demander le bureau
            bureau = input ("Quel est le nouveau bureau? ")
            changer_bureau(labo, nom, bureau)

        elif choix == 4:

            #demander le nom
            nom = input ("Quel est le nom à modifier? ")
            try:
                labo.test_nom_absent(laboratoire , nom)
            except labo.AbsentException:
                    print("Cette personne n'est pas dans le laboratoire")
            #demander le nouvel orthographe
            nouveau_nom = input ("Quel est le nouvel orthographe? ")
            try:
                labo.changer_nom(laboratoire , nom, nouveau_nom)
            except labo.PresentException:
                print("Cette personne est deja dans le laboratoire")

        elif choix == 5:

            #demander le nom
            nom = input ("Quel est le nom de la personne")
            labo.savoir_membre(laboratoire , nom)

        elif choix == 6:

            #demander le nom
            nom = input ("Quel est le nom de la personne")
            labo.savoir_bureau(laboratoire , nom)

        elif choix == 7:

            impression_personnel()

        elif choix == 8:

            labo.afficher_occupation(laboratoire)

        elif choix ==0:
            print("Au revoir")
            quitter = True

        else: 
            print("Je n'ai pas compris")

#fonction d'interface pour tester la presence d'un membre du laboratoire
def ihmmembre(laboratoire):
    def interne():
        nom = input ("Quel est le nom de la personne")
        labo.savoir_membre(laboratoire , nom)
    return interne

#fonction d'interface pour obtenir le bureau d'une personne
def ihmbureau(laboratoire):
    def interne():
        nom = input ("Quel est le nom de la personne")
        labo.savoir_bureau(laboratoire , nom)
    return interne

#fonction d'interface pour afficher l'occupation du laboratoire
def ihmoccup(laboratoire):
    def interne():
        labo.afficher_occupation(laboratoire)
    return interne

#fonction d'interface pour enregistrer une nouvelle arrivée
def ihmarrivee(laboratoire):
    def interne():
        nom = input ("Quel est le nom du nouvel arrivant? ")
        try:
            labo.test_nom_present(laboratoire , nom)
        except labo.PresentException:
                print("Cette personne est deja dans le laboratoire")
        #demander le bureau
        bureau = input ("Quel est le bureau du nouvel arrivant? ")
        #enregistrer la nouvelle personne
        labo.enregistrer_arrivée(laboratoire , nom, bureau)
    return interne

#fonction d'interface pour enregistrer un nouveau départ
def ihmdepart(laboratoire):
    def interne():
        nom = input ("Quel est le nom de la personne qui s'en va? ")
        try:
            labo.enregistrer_depart(laboratoire , nom)
        except labo.AbsentException:
            print("Cette personne n'est pas dans le laboratoire")
    return interne

#fonction d'interface pour enregistrer un changement de bureau
def ihmbureau(laboratoire):
    def interne():
        #demander le nom
        nom = input ("Quel est le nom de la personne qui change de bureau? ")
        try:
            labo.test_nom_absent(laboratoire , nom) 
        except labo.AbsentException:
                print("Cette personne n'est pas dans le laboratoire")
        #demander le bureau
        bureau = input ("Quel est le nouveau bureau? ")
        changer_bureau(labo, nom, bureau)
    return interne

#fonction d'interface pour enregistrer un changement de nom
def ihmnom(laboratoire):
    def interne():
        #demander le nom
        nom = input ("Quel est le nom à modifier? ")
        try:
            labo.test_nom_absent(laboratoire , nom)
        except labo.AbsentException:
                print("Cette personne n'est pas dans le laboratoire")
        #demander le nouvel orthographe
        nouveau_nom = input ("Quel est le nouvel orthographe? ")
        try:
            labo.changer_nom(laboratoire , nom, nouveau_nom)
        except labo.PresentException:
            print("Cette personne est deja dans le laboratoire")
    return interne







main()

